# Electron with create-react-app boilerplate

## Development

```bash
npm install
npm start
```

Implement your backend in `public/backend/`. And your frontend in `src`.

### Styling

Customize [ant design](https://ant.design/components/button/) variables in `antd.customize.less`

Add custom css with [_emotion_](https://emotion.sh):

```jsx harmony
import styled from "@emotion/styled";

const ContentWrapper = styled.div`
  background: #fff;
  padding: 24px;
  min-height: calc(100vh - 64px - 69px);
`;
// ...
const SomeReactComponent = () => {
  return <ContentWrapper>Some Content</ContentWrapper>;
};
```

### Routing

Use [@reach/router](https://reach.tech/router) for routing

```jsx harmony
const App = () => (
  <Router>
    <SomeReactComponent path="some-pathname-in-your-app" />
  </Router>
);

//...
const SomeLink = () => <Link to="some-pathname-in-your-app">Some Link</Link>;
```

## Build

```bash
# Build for mac and windows
npm run build-all
# or
npm run build-mac
npm run build-win
```
