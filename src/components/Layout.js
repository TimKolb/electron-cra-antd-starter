import React from "react";
import { Layout } from "antd";
import styled from "@emotion/styled";
import { AppLogo } from "./AppLogo";
import { AppMenu } from "./AppMenu";

const { Header, Content, Footer } = Layout;

const FullHeightLayout = styled(Layout)`
  min-height: 100vh;
`;

const AppHeader = styled(Header)`
  display: flex;
  position: fixed;
  width: 100%;
`;

const AppContent = styled(Content)`
  margin-top: 64px;
  padding: 0 50px;
`;

const AppFooter = styled(Footer)`
  text-align: center;
`;

const ContentWrapper = styled.div`
  background: #fff;
  padding: 24px;
  min-height: calc(100vh - 64px - 69px);
`;

const YEAR = new Date().getFullYear();

export const AppLayout = ({ children }) => (
  <FullHeightLayout>
    <AppHeader>
      <AppLogo />
      <AppMenu />
    </AppHeader>
    <AppContent>
      <ContentWrapper>{children}</ContentWrapper>
    </AppContent>
    <AppFooter>Kolberger ©{YEAR}</AppFooter>
  </FullHeightLayout>
);
