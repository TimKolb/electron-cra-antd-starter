import React from "react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

const LogoLink = styled(Link)`
  display: block;
  margin: 0 1rem;
  color: #fff;
  font-weight: bolder;
  user-select: none;
`;

export const AppLogo = () => <LogoLink to="/">My App</LogoLink>;
