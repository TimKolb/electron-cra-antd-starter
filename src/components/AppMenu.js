import React from "react";
import { Icon, Menu } from "antd";
import { Location } from "@reach/router";
import styled from "@emotion/styled";

const LayoutMenu = styled(Menu, {
  shouldForwardProp: prop => true
})`
  flex: 1;
  line-height: 64px;
  display: flex;
`;

const MenuItem = Menu.Item;
const LastMenuItem = styled(MenuItem)`
  margin-left: auto;
`;

const SettingsIcon = styled(Icon)`
  margin-right: 0;
`;

export const AppMenu = () => (
  <Location>
    {({ location, navigate }) => (
      <LayoutMenu
        theme="dark"
        mode="horizontal"
        selectedKeys={[location.pathname.split("/")[1]]}
        onSelect={({ key }) => navigate(`/${key}`)}
      >
        <MenuItem key="screen2">
          <Icon type="pie-chart" /> Screen 2
        </MenuItem>
        <LastMenuItem icon="settings" key="settings">
          <SettingsIcon type="setting" />
        </LastMenuItem>
      </LayoutMenu>
    )}
  </Location>
);
