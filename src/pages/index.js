import React, { Component } from "react";
import { Router } from "@reach/router";
import { Home } from "./Home";
import { Settings } from "./Settings";
import { NotFound } from "./NotFound";

export class MainRoute extends Component {
  render() {
    return (
      <Router basepath={window.location.pathname}>
        <Home path="/" />
        <Settings path="/settings" />
        <NotFound default />
      </Router>
    );
  }
}
