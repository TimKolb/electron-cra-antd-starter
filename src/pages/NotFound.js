import React from "react";
import { AppLayout } from "../components/Layout";
import { Empty } from "antd";

export const NotFound = () => {
  return (
    <AppLayout>
      <Empty description="Oops. You found nothing. This page does not exist." />
    </AppLayout>
  );
};
