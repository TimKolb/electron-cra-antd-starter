import React from "react";
import { AppLayout } from "../components/Layout";
import { Link } from "@reach/router";

export const Home = () => {
  return (
    <AppLayout>
      <h1>Home</h1>
      <Link to="settings">Go to settings</Link>
    </AppLayout>
  );
};

const { ipcRenderer } = window.require("electron");
ipcRenderer.send('log')