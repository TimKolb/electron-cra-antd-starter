const { ipcMain } = require('electron');

module.exports = function publishFunctionOfObject(target) {
    Object.keys(target).forEach(key => {
        if (typeof target[key] !== 'function') {
            return;
        }
        ipcMain.on(key, async (event, args) => {
            console.log(
                '[publishFunction]',
                key,
                '[Args]',
                typeof args === 'object' ? JSON.stringify(args, null, 2) : args,
            );
            const result = await target[key](event, args);
            console.log('[publishFunction]', key, '[Result]', result);
            event.sender.send(`${key}-finished`, result);
        });
    });
};
